import {MigrationInterface, QueryRunner} from "typeorm";

export class sessionId1588380238460 implements MigrationInterface {
    name = 'sessionId1588380238460'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "room" ADD "sessionId" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "room" ADD CONSTRAINT "UQ_22b368ef0e572196e67f6c368d3" UNIQUE ("sessionId")`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "room" DROP CONSTRAINT "UQ_22b368ef0e572196e67f6c368d3"`, undefined);
        await queryRunner.query(`ALTER TABLE "room" DROP COLUMN "sessionId"`, undefined);
    }

}
