import {MigrationInterface, QueryRunner} from "typeorm";

export class lastSeen1588358160586 implements MigrationInterface {
    name = 'lastSeen1588358160586'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "player" ADD "lastSeen" TIMESTAMP`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "player" DROP COLUMN "lastSeen"`, undefined);
    }

}
