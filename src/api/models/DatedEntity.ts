import { CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

export abstract class KeyedEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
}

export abstract class DatedEntity extends KeyedEntity {
  @CreateDateColumn({ type: 'timestamp' })
  created_at: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updated_at: Date;
}
