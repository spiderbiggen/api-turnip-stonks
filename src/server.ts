import express, { Request, Response } from 'express';
import 'reflect-metadata';
import { createConnection } from 'typeorm';
import { ROUTE_REGISTRY } from './routing';
import routes from './api/routes';
import { Authentication, LOGGER, NotFoundError, ResponseHelper, SocketInstance } from './util';
import bodyParser from 'body-parser';
import compression from 'compression';
import helmet from 'helmet';
import cors from 'cors';
import http from 'http';

const port = process.env.PORT || 3000;
const prodEnv = process.env.NODE_ENV === 'production';

/**
 * Create a database connection then load all routes. Then create the server.
 *
 * @author Stefan Breetveld
 */
createConnection()
  .then(() => ROUTE_REGISTRY.load(routes))
  .then(() => {
    const app = express();
    app.use(compression());
    app.use(helmet());
    app.use(cors());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    // Get User from token if available, for all routes
    app.use(Authentication.userMiddleware);

    app.set('json spaces', prodEnv ? 0 : 2);

    const root = process.env.NODE_ROOT || '';
    ROUTE_REGISTRY.registerRoutes(app, root);
    app.use((req: Request, res: Response) => ResponseHelper.sendError(req, res, new NotFoundError()));

    const server = http.createServer(app)
    SocketInstance.init(server);
    server.listen(port);
    LOGGER.info('API Running on :%s/%s', port, root ?? '');
  }).catch(err => {
  LOGGER.error(err);
});

