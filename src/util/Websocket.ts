import Socket from 'socket.io';
import { Server } from 'http';
import Sockets from '../api/websockets';

class WebSocket {
  private _socket?: Socket.Server;

  get socket() {
    return this._socket;
  }

  init(server: Server) {
    const socket = Socket(server);
    this._socket = socket;
    Sockets.forEach(s => s.init(socket));
  }
}

export const SocketInstance = new WebSocket();
