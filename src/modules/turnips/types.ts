import { Player, Room } from './models';

export type QueueSocketMessage = Partial<Player> & { sessionId: string };
export type HostSocketMessage = Partial<Room>;
export type RoomUpdate = { room_id: string, max_users: number, current_users: number, accepted: Player[], island: Partial<Room>, message?: string | null };
