import { QueueSocketMessage, RoomUpdate } from '../types';
import { Player, Room } from '../models';
import { LOGGER } from '../../../util';
import TurnipSocket from '../websockets/TurnipSocket';

export module TurnipController {
  export async function registerPlayer(object: QueueSocketMessage): Promise<Player> {
    const roomRepository = Room.repository;
    const room = await roomRepository.findOneOrFail({ sessionId: object.sessionId });
    LOGGER.info(`Found room for sessionId %s: %s`, object.sessionId, JSON.stringify(room));
    const player = await Player.register(object);
    player.room = room;
    return Player.repository.save(player);
  }

  export async function updateRoom(room?: Room) {
    LOGGER.verbose('Updating room state for %s', room?.id)
    if (!room) return;
    const repository = Room.repository;
    const roomData = await repository.findOneOrFail(room.id, { relations: ['players'] });
    const update: RoomUpdate = {
      accepted: roomData.players.filter(p => p.invited),
      current_users: roomData.players.length,
      island: { username: roomData.username, island: roomData.island },
      max_users: 0, // TODO add max queue size
      room_id: room.sessionId,
    };
    TurnipSocket.publish(room, update);
  }
}
