import { Column, Entity, getRepository, ManyToOne } from 'typeorm';
import { DatedEntity } from '../../../api/models/DatedEntity';
import { Room } from './Room';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
@Entity()
export class Player extends DatedEntity {

  @Column({ nullable: false })
  username!: string;
  @Column({ nullable: false })
  island!: string;
  @Column({ nullable: false })
  socketId!: string;
  @Column({ default: false })
  invited: false;
  @Column({ nullable: true })
  lastSeen?: Date;
  @ManyToOne(() => Room, room => room.players, { onDelete: 'CASCADE' })
  room?: Room;


  static get repository() {
    return getRepository(Player);
  }

  static async register(object: any): Promise<Player> {
    if (object == null || typeof object !== 'object' || Array.isArray(object)) throw new Error('NOT_A_VALID_PLAYER');
    if (!object?.username || typeof object.username !== 'string') throw new Error('NOT_A_VALID_PLAYER');
    if (!object?.island || typeof object.island !== 'string') throw new Error('NOT_A_VALID_PLAYER');
    if (!object?.socketId || typeof object.socketId !== 'string') throw new Error('NOT_A_VALID_PLAYER');
    const player = {
      username: object.username,
      island: object.island,
      socketId: object.socketId,
    };
    const entity = await this.repository.create(player);
    return this.repository.save(entity);
  }

  toJSON() {
    return {
      username: this.username,
      island: this.island,
      invited: this.invited,
    };
  }
}
