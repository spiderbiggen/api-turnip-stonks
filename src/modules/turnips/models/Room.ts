import { Column, Entity, getRepository, OneToMany } from 'typeorm';
import { Player } from './Player';
import { DatedEntity } from '../../../api/models';
import { generateBase64Salt } from '../../../util';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
@Entity()
export class Room extends DatedEntity {

  @Column({ nullable: false })
  username!: string;
  @Column({ nullable: false })
  island!: string;
  @Column({ nullable: false })
  dodo!: string;
  @Column({ nullable: false, unique: true })
  sessionId: string;
  @Column({ nullable: false })
  socketId!: string;
  @OneToMany(() => Player, player => player.room, { cascade: true, onDelete: 'SET NULL' })
  players!: Player[];

  static get repository() {
    return getRepository(Room);
  }

  static async register(object: any): Promise<Room> {
    if (object == null || typeof object !== 'object' || Array.isArray(object)) throw new Error('NOT_A_VALID_ROOM');
    if (!object?.username || typeof object.username !== 'string') throw new Error('NOT_A_VALID_ROOM');
    if (!object?.island || typeof object.island !== 'string') throw new Error('NOT_A_VALID_ROOM');
    if (!object?.socketId || typeof object.socketId !== 'string') throw new Error('NOT_A_VALID_ROOM');
    if (!object?.dodo || typeof object.dodo !== 'string') throw new Error('NOT_A_VALID_ROOM');
    const room = {
      username: object.username,
      island: object.island,
      socketId: object.socketId,
      sessionId: await generateBase64Salt(8),
      dodo: object.dodo,
    };
    const entity = await this.repository.create(room);
    return this.repository.save(entity);
  }

  toJSON() {
    return {
      username: this.username,
      island: this.island,
      sessionId: this.sessionId,
    };
  }
}
