import { Socket } from 'socket.io';
import { Room } from '../models';
import { LOGGER } from '../../../util';
import { TurnipController } from '../controllers/TurnipController';
import { HostSocketMessage, QueueSocketMessage } from '../types';

//
// ooooo       ooooo  oooooooo8 ooooooooooo ooooooooooo oooo   oooo ooooooooooo oooooooooo
//  888         888  888        88  888  88  888    88   8888o  88   888    88   888    888
//  888         888   888oooooo     888      888ooo8     88 888o88   888ooo8     888oooo88
//  888      o  888          888    888      888    oo   88   8888   888    oo   888  88o
// o888ooooo88 o888o o88oooo888    o888o    o888ooo8888 o88o    88  o888ooo8888 o888o  88o8
//
/**
 * Handles events sent and received by a single socket eg a user.
 */
export class TurnipEventListener {
  private readonly socket: Socket;
  private player: string;
  private room: string;

  private constructor(socket: Socket) {
    this.socket = socket;
  }

  static createListener(socket: Socket): TurnipEventListener {
    const handler = new TurnipEventListener(socket);
    LOGGER.verbose(`New Socket connection to namespace /turnips with id ${ socket.id }`);
    handler.initListeners();
    return handler;
  }

  private initListeners() {
    this.socket.on('join', (a) => this.onJoin(a));
    this.socket.on('host', (a) => this.onHost(a));
    this.socket.on('queue', (a) => this.onQueue(a));
    this.socket.on('leave', (a) => this.onLeave(a));
    this.socket.on('end', (a) => this.onEnd(a));
    this.socket.on('update', (a) => this.onUpdate(a));
    this.socket.on('disconnect', (a) => this.onDisconnect(a));
  }

  //
  //   ooooooo8 ooooooooooo oooo   oooo ooooooooooo oooooooooo       o      ooooo
  // o888    88  888    88   8888o  88   888    88   888    888     888      888
  // 888    oooo 888ooo8     88 888o88   888ooo8     888oooo88     8  88     888
  // 888o    88  888    oo   88   8888   888    oo   888  88o     8oooo88    888      o
  //  888ooo888 o888ooo8888 o88o    88  o888ooo8888 o888o  88o8 o88o  o888o o888ooooo88
  //
  private async onJoin(a: any) {
    // This should rejoin the user to the correct entity
    LOGGER.debug('[T-SOCKET] joined: %s (%s)', a.username, a.sessionId);
    // TODO validate sessionID
    this.socket.join(a.sessionId);
  }

  private async onDisconnect(a: any) {
    LOGGER.info(`${ this.socket.id } disconnected`);
  }

  //
  // oooooooooo ooooo            o   ooooo  oooo ooooooooooo oooooooooo
  //  888    888 888            888    888  88    888    88   888    888
  //  888oooo88  888           8  88     888      888ooo8     888oooo88
  //  888        888      o   8oooo88    888      888    oo   888  88o
  // o888o      o888ooooo88 o88o  o888o o888o    o888ooo8888 o888o  88o8
  //
  private async onQueue(a: QueueSocketMessage) {
    this.socket.join(a.sessionId);
    try {

    LOGGER.info(`queing for ${ JSON.stringify(a) }`);
    a.socketId = this.socket.id;
    const result = await TurnipController.registerPlayer(a);
    this.player = result.id;
    console.log(a.sessionId);
    this.socket.emit('joined', Object.assign({}, result));
    await TurnipController.updateRoom(result.room);
    } catch (e) {
      this.socket.leave(a.sessionId, (err: any) => LOGGER.error(err))
    }
  }

  private async onLeave(a: any) {
    LOGGER.info(`leaving for ${ JSON.stringify(a) }`);
  }

  //
  // ooooo ooooo  ooooooo    oooooooo8 ooooooooooo
  //  888   888 o888   888o 888        88  888  88
  //  888ooo888 888     888  888oooooo     888
  //  888   888 888o   o888         888    888
  // o888o o888o  88ooo88   o88oooo888    o888o
  //
  private async onHost(a: HostSocketMessage) {
    LOGGER.info(`hosting ${ a.dodo }`);
    a.socketId = this.socket.id;
    const result = a.id ? await Room.repository.save(a) : await Room.register(a);
    this.room = result.id;
    this.socket.join(result.sessionId);
    this.socket.emit('joined', Object.assign({}, result));
  }

  private async onUpdate(a: any) {
    LOGGER.info(`changing for ${ JSON.stringify(a) }`);
  }

  private async onEnd(a: any) {
    LOGGER.info(`ending for ${ JSON.stringify(a) }`);
  }
}
