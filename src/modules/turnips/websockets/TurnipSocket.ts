import { Namespace, Server } from 'socket.io';
import { LOGGER } from '../../../util';
import { Player, Room } from '../models';
import { TurnipEventListener } from './TurnipEventListener';
import { RoomUpdate } from '../types';

//
//  oooooooo8    ooooooo     oooooooo8 oooo   oooo ooooooooooo ooooooooooo
// 888         o888   888o o888     88  888  o88    888    88  88  888  88
//  888oooooo  888     888 888          888888      888ooo8        888
//         888 888o   o888 888o     oo  888  88o    888    oo      888
// o88oooo888    88ooo88    888oooo88  o888o o888o o888ooo8888    o888o
//
class TurnipSocket {
  private socket: Server;
  private namespace: Namespace;

  init(server: Server) {
    this.socket = server;
    this.namespace = server.of('/turnips');
    this.namespace.on('connection', socket => TurnipEventListener.createListener(socket));
    LOGGER.verbose('Initialised /turnips namespace');
  }

  /**
   * Publish an update to a room
   * @param room
   * @param data
   */
  publish(room: Room, data: RoomUpdate) {
    if (!this.namespace) {
      LOGGER.warn('Turnips Websocket namespace was not initialized');
      return;
    }

    LOGGER.verbose('Updating room %s', room.sessionId)
    this.namespace.in(room.sessionId).emit('update', data);
  }

  accept(room: Room, user: Player) {
    if (!this.namespace) {
      LOGGER.warn('Turnips Websocket namespace was not initialized');
      return;
    }
    if (room.dodo.length <= 0) return;

    this.socket.to(user.socketId).emit('accepted', { code: room.dodo });
  }
}

export default new TurnipSocket();
